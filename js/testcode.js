// load config file
var request = new XMLHttpRequest();
request.open("GET", "settings.json", false);
request.send(null);
var config = JSON.parse(request.responseText);
if(config != 'undefined'){console.log("Configuration file loaded successfully.");}

currentClockObj = new Date();
lat = (-70.53444)*2*Math.PI/360;
lon = (-33.2692)*2*Math.PI/360;

function azel2eq(az,el){
	//at = config.LOCATION.LAT;
	//lon = config.LOCATION.LONG;
	dec = Math.acos(Math.sin(lat)*Math.sin(el)+Math.cos(lat)*Math.cos(el)*Math.cos(az));
	han = (Math.sin(el)-Math.sin(dec)*Math.sin(lat))/(Math.cos(dec)*Math.cos(lat));
	lst = currentClockObj.getHours()+currentClockObj.getMinutes()/60+currentClockObj.getSeconds()/3600;
	ra = lst-han;
	return [ra,dec];
}

console.log(azel2eq(0,0));